//
//  RJRDetailViewController.h
//  WorldCup
//
//  Created by Robert Randell on 15/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJRDetailViewController : UIViewController

@property (nonatomic) NSString *_matchID;

@property (weak, nonatomic) IBOutlet UIImageView *homeBadgeImage;
@property (weak, nonatomic) IBOutlet UIImageView *awayBadgeImage;

@property (weak, nonatomic) IBOutlet UILabel *fullTimeScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeTeamLabel;
@property (weak, nonatomic) IBOutlet UILabel *awayTeamLabel;

@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UILabel *attendanceLabel;

@property (weak, nonatomic) IBOutlet UIView *homeLineUpView;
@property (weak, nonatomic) IBOutlet UIView *awayLineUpView;
@property (weak, nonatomic) IBOutlet UIView *homeScorersView;
@property (weak, nonatomic) IBOutlet UIView *awayScorersView;
@property (weak, nonatomic) IBOutlet UIView *timelineView;

@property (weak, nonatomic) NSString *_homeTeamString;
@property (weak, nonatomic) NSArray *_homeTeamLineup;
@property (weak, nonatomic) NSArray *_homeScorers;
@property (weak, nonatomic) NSMutableArray *_homeGoalsTime;

@property (weak, nonatomic) NSString *_awayTeamString;
@property (weak, nonatomic) NSArray *_awayTeamLineup;
@property (weak, nonatomic) NSArray *_awayScorers;
@property (weak, nonatomic) NSMutableArray *_awayGoalsTime;

@property (weak, nonatomic) NSString *_venueString;
@property (nonatomic) double _attendanceValue;

@end