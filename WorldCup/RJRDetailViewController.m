//
//  RJRDetailViewController.m
//  WorldCup
//
//  Created by Robert Randell on 15/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRDetailViewController.h"

@interface RJRDetailViewController ()

@end

@implementation RJRDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    
    self._homeGoalsTime = [NSMutableArray array];
    self._awayGoalsTime = [NSMutableArray array];
    
    self.homeTeamLabel.text = self._homeTeamString;
    self.awayTeamLabel.text = self._awayTeamString;
    self.venueLabel.text = self._venueString;
    self.attendanceLabel.text = [NSString stringWithFormat:@"%.0f", self._attendanceValue];
    
    for (int x = 0; x < [self._homeTeamLineup count]; x++) {
        
        int posY = 20 * x;
        
        NSString *playerName = [[self._homeTeamLineup objectAtIndex:x] objectForKey:@"name"];
        NSNumber *playerNo = [[self._homeTeamLineup objectAtIndex:x] objectForKey:@"number"];
        
        UILabel *playerNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY, 20, 20)];
        playerNoLabel.text = [NSString stringWithFormat:@"%@", playerNo];
        playerNoLabel.textAlignment = NSTextAlignmentRight;
        [playerNoLabel setFont:[UIFont systemFontOfSize:12]];
        
        UILabel *playerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, posY, 150, 20)];
        playerNameLabel.text = [NSString stringWithFormat:@"%@", playerName];
        playerNameLabel.textAlignment = NSTextAlignmentLeft;
        [playerNameLabel setFont:[UIFont systemFontOfSize:12]];

        [self.homeLineUpView addSubview:playerNoLabel];
        [self.homeLineUpView addSubview:playerNameLabel];
        
    }
    
    for (int x = 0; x < [self._awayTeamLineup count]; x++) {
        
        int posY = 20 * x;
        
        NSString *playerName = [[self._awayTeamLineup objectAtIndex:x] objectForKey:@"name"];
        NSNumber *playerNo = [[self._awayTeamLineup objectAtIndex:x] objectForKey:@"number"];
        
        UILabel *playerNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, posY, 20, 20)];
        playerNoLabel.text = [NSString stringWithFormat:@"%@", playerNo];
        playerNoLabel.textAlignment = NSTextAlignmentLeft;
        [playerNoLabel setFont:[UIFont systemFontOfSize:12]];
        
        UILabel *playerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY, 105, 20)];
        playerNameLabel.text = [NSString stringWithFormat:@"%@", playerName];
        playerNameLabel.textAlignment = NSTextAlignmentRight;
        [playerNameLabel setFont:[UIFont systemFontOfSize:12]];

        [self.awayLineUpView addSubview:playerNoLabel];
        [self.awayLineUpView addSubview:playerNameLabel];

    }
    
    for (int x = 0; x < [self._homeScorers count]; x++) {
        
        int posY = 10 * x;
        
        NSString *homeScorer = [[self._homeScorers objectAtIndex:x] objectForKey:@"name"];
        NSNumber *homeScorerTime = [[self._homeScorers objectAtIndex:x] objectForKey:@"time"];
        
        UILabel *homeScorersLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY, self.homeScorersView.bounds.size.width, 10)];
        homeScorersLabel.text = [NSString stringWithFormat:@"%@ %@'", homeScorer, homeScorerTime];
        homeScorersLabel.textAlignment = NSTextAlignmentLeft;
        [homeScorersLabel setFont:[UIFont systemFontOfSize:10]];
        
        [self.homeScorersView addSubview:homeScorersLabel];
        [self._homeGoalsTime addObject:homeScorerTime];
    }
    
    for (int x = 0; x < [self._awayScorers count]; x++) {
        
        int posY = 10 * x;
        
        NSString *awayScorer = [[self._awayScorers objectAtIndex:x] objectForKey:@"name"];
        NSNumber *awayScorerTime = [[self._awayScorers objectAtIndex:x] objectForKey:@"time"];
        
        UILabel *awayScorersLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY, self.homeScorersView.bounds.size.width, 10)];
        awayScorersLabel.text = [NSString stringWithFormat:@"%@ %@'", awayScorer, awayScorerTime];
        awayScorersLabel.textAlignment = NSTextAlignmentRight;
        [awayScorersLabel setFont:[UIFont systemFontOfSize:10]];
        
        [self.awayScorersView addSubview:awayScorersLabel];
        [self._awayGoalsTime addObject:awayScorerTime];

    }
    
    for (int x = 0; x < [self._homeGoalsTime count]; x++) {
        
        int goalTime = [[self._homeGoalsTime objectAtIndex:x] integerValue];
        int matchTime = 90;
        int matchTimelineWidth = self.timelineView.bounds.size.width;
        int matchUnit = matchTimelineWidth / matchTime;
        int posX = matchUnit * goalTime;
        
        UIImageView *goalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(posX, 0, 10, 10)];
        goalImageView.backgroundColor = [UIColor blueColor];
        
        [self.timelineView addSubview:goalImageView];
        
    }
    
    for (int x = 0; x < [self._awayGoalsTime count]; x++) {
        
        int goalTime = [[self._awayGoalsTime objectAtIndex:x] integerValue];
        int matchTime = 90;
        int matchTimelineWidth = self.timelineView.bounds.size.width;
        int matchUnit = matchTimelineWidth / matchTime;
        int posX = matchUnit * goalTime;
        
        UIImageView *goalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(posX, 40, 10, 10)];
        goalImageView.backgroundColor = [UIColor greenColor];
        
        [self.timelineView addSubview:goalImageView];
        
    }
}

@end