//
//  RJRResultCell.h
//  WorldCup
//
//  Created by Robert Randell on 14/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJRResultCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *homeTeamLabel;
@property (nonatomic, weak) IBOutlet UILabel *awayTeamLabel;
@property (nonatomic, weak) IBOutlet UILabel *finalScore;
@property (nonatomic, weak) IBOutlet UIImageView *homeTeamBadge;
@property (nonatomic, weak) IBOutlet UIImageView *awayTeamBadge;
@property (nonatomic, weak) IBOutlet UILabel *matchDateLabel;

@end
