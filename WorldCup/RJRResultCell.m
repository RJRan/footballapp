//
//  RJRResultCell.m
//  WorldCup
//
//  Created by Robert Randell on 14/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRResultCell.h"

@implementation RJRResultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
