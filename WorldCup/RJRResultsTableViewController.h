//
//  RJRResultsTableViewController.h
//  WorldCup
//
//  Created by Robert Randell on 11/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJRResultsTableViewController : UITableViewController

@property (nonatomic) NSMutableDictionary *_worldCupTableData;

@property (nonatomic) NSMutableArray *_groupAData;
@property (nonatomic) NSMutableArray *_groupBData;

- (NSString *)convertFromUNIX:(double)UNIXTime;

@end
