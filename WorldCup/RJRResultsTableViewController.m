//
//  RJRResultsTableViewController.m
//  WorldCup
//
//  Created by Robert Randell on 11/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRResultsTableViewController.h"
#import "RJRResultCell.h"
#import "RJRDetailViewController.h"
#import "RJRWorldCupData.h"

@interface RJRResultsTableViewController ()

@end

@implementation RJRResultsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Results";
    
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"worldcupdata" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:filepath];
    NSError *error = nil;
    NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    self._worldCupTableData = [NSMutableDictionary dictionaryWithDictionary:dataDictionary];
    
    self._groupAData = [NSMutableArray array];
    self._groupBData = [NSMutableArray array];
    
    NSArray *matchInfoArrayGroupA = [[[[self._worldCupTableData objectForKey:@"groups"] objectForKey:@"results"] objectAtIndex:0] objectForKey:@"group"];
    NSArray *matchInfoArrayGroupB = [[[[self._worldCupTableData objectForKey:@"groups"] objectForKey:@"results"] objectAtIndex:1] objectForKey:@"group"];
    
    for (NSDictionary *matchInfoDictionaryGroupA in matchInfoArrayGroupA) {
        
        RJRWorldCupData *matchData = [RJRWorldCupData matchObjectWithMatch:[[matchInfoDictionaryGroupA objectForKey:@"group"] objectForKey:@"match"]];
        
        matchData.matchIdentifier = [matchInfoDictionaryGroupA objectForKey:@"match"];
        
        matchData.homeTeamString = [[matchInfoDictionaryGroupA objectForKey:@"homelineup"] objectForKey:@"shortcode"];
        matchData.homeTeamFullString = [[matchInfoDictionaryGroupA objectForKey:@"homelineup"] objectForKey:@"hometeam"];
        matchData.homeTeamLineup = [[matchInfoDictionaryGroupA objectForKey:@"homelineup"] objectForKey:@"lineup"];
        matchData.homeScore = [[matchInfoDictionaryGroupA objectForKey:@"homelineup"] objectForKey:@"goals"];
        matchData.homeBadge = [UIImage imageNamed:[[matchInfoDictionaryGroupA objectForKey:@"homelineup"] objectForKey:@"shortcode"]];
        matchData.homeScorersArray = [[matchInfoDictionaryGroupA objectForKey:@"homelineup"] objectForKey:@"scorers"];
        
        matchData.awayTeamString = [[matchInfoDictionaryGroupA objectForKey:@"awaylineup"] objectForKey:@"shortcode"];
        matchData.awayTeamFullString = [[matchInfoDictionaryGroupA objectForKey:@"awaylineup"] objectForKey:@"awayteam"];
        matchData.awayTeamLineup = [[matchInfoDictionaryGroupA objectForKey:@"awaylineup"] objectForKey:@"lineup"];
        matchData.awayScore = [[matchInfoDictionaryGroupA objectForKey:@"awaylineup"] objectForKey:@"goals"];
        matchData.awayBadge = [UIImage imageNamed:[[matchInfoDictionaryGroupA objectForKey:@"awaylineup"] objectForKey:@"shortcode"]];
        matchData.awayScorersArray = [[matchInfoDictionaryGroupA objectForKey:@"awaylineup"] objectForKey:@"scorers"];
        
        matchData.matchDate = [self convertFromUNIX:[[matchInfoDictionaryGroupA objectForKey:@"date"] doubleValue]];
        matchData.attendanceValue = [[matchInfoDictionaryGroupA objectForKey:@"attendance"] doubleValue];
        matchData.venueString = [matchInfoDictionaryGroupA objectForKey:@"venue"];

        
        NSLog(@"Group A - ID: %@, %@ %@-%@ %@ %@", matchData.matchIdentifier, matchData.homeTeamString, matchData.homeScore, matchData.awayScore, matchData.awayTeamString, matchData.matchDate);

        [self._groupAData addObject:matchData];
        
    }
    
    for (NSDictionary *matchInfoDictionaryGroupB in matchInfoArrayGroupB) {
        
        RJRWorldCupData *matchData = [RJRWorldCupData matchObjectWithMatch:[[matchInfoDictionaryGroupB objectForKey:@"group"] objectForKey:@"match"]];
        matchData.homeTeamString = [[matchInfoDictionaryGroupB objectForKey:@"homelineup"] objectForKey:@"shortcode"];
        matchData.awayTeamString = [[matchInfoDictionaryGroupB objectForKey:@"awaylineup"] objectForKey:@"shortcode"];
        
        NSLog(@"Group B: %@ vs. %@", matchData.homeTeamString, matchData.awayTeamString);
        
        [self._groupBData addObject:matchData];
        
    }
}

#pragma mark - Custom methods

- (NSString *)convertFromUNIX:(double)UNIXTime {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:UNIXTime];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    return [dateformatter stringFromDate:date];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 6;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *sectionName;
    
    switch (section) {
        case 0:
            sectionName = @"Group A";
            break;
        case 1:
            sectionName = @"Group B";
            break;
        case 2:
            sectionName = @"Group C";
            break;
        case 3:
            sectionName = @"Group D";
            break;
        case 4:
            sectionName = @"Group E";
            break;
        case 5:
            sectionName = @"Group F";
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in each section.
    
    int numberOfRows;
    
    switch (section) {
        case 0:
            numberOfRows = [self._groupAData count];
            break;
        case 1:
            numberOfRows = [self._groupBData count];
            break;
        default:
            numberOfRows = 0;
            break;
    }
    return numberOfRows;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    RJRResultCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSArray *chosenMatchArray = [NSArray array];
    
    //Configure the cell...
    
    switch (indexPath.section) {
        case 0:
            chosenMatchArray = [NSArray arrayWithArray:self._groupAData];
            break;
        case 1:
            chosenMatchArray = [NSArray arrayWithArray:self._groupBData];
            break;
        default:
            break;
    }
    
    RJRWorldCupData *matchData = [chosenMatchArray objectAtIndex:indexPath.row];
    
    cell.finalScore.text = [NSString stringWithFormat:@"%@-%@", matchData.homeScore, matchData.awayScore];
    cell.homeTeamLabel.text = matchData.homeTeamString;
    cell.awayTeamLabel.text = matchData.awayTeamString;
    cell.matchDateLabel.text = matchData.matchDate;
    cell.homeTeamBadge.image = matchData.homeBadge;
    cell.awayTeamBadge.image = matchData.awayBadge;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSArray *chosenMatchArray = [NSArray array];
    
    if ([segue.identifier isEqualToString:@"detailMatchView"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        switch (indexPath.section) {
            case 0:
                chosenMatchArray = [NSArray arrayWithArray:self._groupAData];
                break;
            case 1:
                chosenMatchArray = [NSArray arrayWithArray:self._groupBData];
                break;
            default:
                break;
        }
        
        RJRDetailViewController *detailViewController = (RJRDetailViewController *)segue.destinationViewController;
        
        RJRWorldCupData *matchDetails = [chosenMatchArray objectAtIndex:indexPath.row];

        detailViewController._matchID = matchDetails.matchIdentifier;
        detailViewController._homeTeamString = matchDetails.homeTeamFullString;
        detailViewController._awayTeamString = matchDetails.awayTeamFullString;
        
        detailViewController._homeTeamLineup = matchDetails.homeTeamLineup;
        detailViewController._awayTeamLineup = matchDetails.awayTeamLineup;
        
        detailViewController._homeScorers = matchDetails.homeScorersArray;
        detailViewController._awayScorers = matchDetails.awayScorersArray;
        
        detailViewController._attendanceValue = matchDetails.attendanceValue;
        detailViewController._venueString = matchDetails.venueString;

    }
}


/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    RJRResultCell *cell = [tableView cellForRowAtIndexPath:indexPath];

}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
