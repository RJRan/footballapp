//
//  RJRWorldCupData.h
//  WorldCup
//
//  Created by Robert Randell on 15/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RJRWorldCupData : NSObject

@property (nonatomic) NSString *matchIdentifier;

@property (nonatomic) NSString *homeTeamString;
@property (nonatomic) NSString *homeTeamFullString;
@property (nonatomic) NSNumber *homeScore;
@property (nonatomic) NSArray *homeScorersArray;
@property (nonatomic) NSArray *homeTeamLineup;

@property (nonatomic) NSString *awayTeamString;
@property (nonatomic) NSString *awayTeamFullString;
@property (nonatomic) NSNumber *awayScore;
@property (nonatomic) NSArray *awayScorersArray;
@property (nonatomic) NSArray *awayTeamLineup;

@property (nonatomic) UIImage *homeBadge;
@property (nonatomic) UIImage *awayBadge;

@property (nonatomic) NSString *matchDate;
@property (nonatomic) NSString *venueString;
@property (nonatomic) double attendanceValue;

- (id)initWithMatch:(NSString *)matchIdentifier;
+ (id)matchObjectWithMatch:(NSString *)matchIdentifier;

@end