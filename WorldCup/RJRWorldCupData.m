//
//  RJRWorldCupData.m
//  WorldCup
//
//  Created by Robert Randell on 15/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRWorldCupData.h"

@implementation RJRWorldCupData

- (id)initWithMatch:(NSString *)matchIdentifier {
    
    self = [super init];
    
    if (self) {
        _matchIdentifier = matchIdentifier;
    }
    return self;
}

+ (id)matchObjectWithMatch:(NSString *)matchIdentifier {
    
    return [[self alloc] initWithMatch:matchIdentifier];
    
}

@end